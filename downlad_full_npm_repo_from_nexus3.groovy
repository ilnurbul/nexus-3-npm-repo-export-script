#!/usr/bin/env groovy

import java.lang.*;
//import java.io.*;
import groovy.json.JsonSlurper;
import java.io.File;


try {
    N_PAGES_MAX = 295
    List<String> artifacts = new ArrayList<String>()
    repoUrl = "http://nexus.example.com/service/rest/v1/components?repository=npm_proxy"
    ( cutted_var, artifactRepoName ) = repoUrl.split( '=')
    println("[DEBUG] Current repo name: " + artifactRepoName)
    currentPage = 1

    def repo_artifacts_dir = new File(artifactRepoName)
    if (! repo_artifacts_dir.exists()){
        repo_artifacts_dir.mkdir();
        // If you require it to make the entire directory path including parents,
        // use directory.mkdirs(); here instead.
    }

    while (true) {

        def artifactsObjectRawCommand = ["curl", "-s", "-H", "accept: application/json", "${repoUrl}"]
        def stdout = new StringBuffer(), stderr = new StringBuffer()
        def artifactsObjectRawProc =  artifactsObjectRawCommand.execute()
        artifactsObjectRawProc.consumeProcessOutput(stdout, stderr)
        artifactsObjectRawProc.waitForOrKill(30000)
        if (stderr) {
            println(stderr.toString().trim())
            println(stdout.toString().trim())
        }
        def artifactsJsonObjectReturn = stdout.toString().trim()
        artifactsJsonObject = (new JsonSlurper()).parseText(artifactsJsonObjectReturn)
        continuationToken = artifactsJsonObject.continuationToken

        if (continuationToken!=null) {
            if (repoUrl.contains( '&continuationToken') ) {
               (repoUrl, old_token)  = repoUrl.split( '&')
                //println(repoUrl)
            }

            repoUrl = repoUrl + "&continuationToken=$continuationToken"
            //println("[DEBUG] Getting artifacts for: " + repoUrl + " Page: " + currentPage)
        }

        def items = artifactsJsonObject.items
        for(item in items) {
            artifacts.add(items.assets.downloadUrl)
                //String[] artifactPathLong1;
                artifactPathLong1 = item.assets.path
                artifactPathLong = artifactPathLong1.join(',')
                //println(artifactPathLong)
            (artifactPath, artifactName) = artifactPathLong.split("/-/");
                //println(artifactPath)
                //println(artifactName)
            def artifacts_dir = new File(artifactRepoName + "/" + artifactPath);
                //println(artifacts_dir)
            if (! artifacts_dir.exists()) {
                artifacts_dir.mkdirs();
                // If you require it to make the entire directory path including parents,
                // use directory.mkdirs(); here instead.
            }

            def artifactFullPath = new File(artifactRepoName + "/" + artifactPath + "/" + artifactName);
            println(artifactFullPath)

            downloadUrl1 = item.assets.downloadUrl
            downloadUrl = downloadUrl1.join(',')
                //println(downloadUrl)
               def processDownloadCommand = [ "curl", "-s", "-o", "${artifactFullPath}", "${downloadUrl}"]
               processDownloadProc = processDownloadCommand.execute()
                processDownloadProc.consumeProcessOutput(stdout, stderr)
                processDownloadProc.waitForOrKill(300000)
               if (stderr) {
                   println(stderr.toString().trim())
                   println(stdout.toString().trim())

               }
        }

        currentPage += 1
        //sleep(2000)
        if (continuationToken==null || currentPage>N_PAGES_MAX) break
    }
    println ("[DEBUG] Скачано артефактов: " + artifacts.size())
    return artifacts.sort()
}


catch (Exception e) {
    print "There was a problem fetching the urls\n\r"
    e.printStackTrace()
    println("[ERROR] Failed link:\n\r" + repoUrl + "\n\r")
}
